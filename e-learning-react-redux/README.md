# Apprendre à créer une application web avec React et Redux
Cette formation est composée de 14 modules :
* Module 1 : Introduction et Technologies de base
* Module 2 : Configurer l'environnement
* Module 3 : React, approche orientée composant
* Module 4 : Structure initiale de l'application
* Module 5 : Introduction à Redux
* Module 6 : Actions, Stores et Reducers
* Module 7 : Connecter React à Redux
* Module 8 : Flux Redux
* Module 8 : Asynch dans Redux
* Module 10 : Asynch ecrit dans Redux
* Module 11 : Async status et gestion des erreurs
* Module 12 : Tests dans React
* Module 13 : Tests dans Redux
* Module 14 : Mettre en production

