# Apprendre à créer une application web avec React et Flux
Cette formation est composée de 10 modules :
* Module 1 : Introduction et Technologies de base
* Module 2 : Configurer l'environnement
* Module 3 : React, concepts de base
* Module 4 : Composants React
* Module 5 : Cycle de vie React
* Module 6 : Composition React
* Module 7 : Routeur React
* Module 8 : Formulaire React
* Module 8 : Flux
* Module 10 : Demonstration Flux

# Pour démarrer
1. Installer NodeJS (version 8.x.x)
2. Telecharger le repo
3. Installer les paquets (npm install)
4. Lancer l'appli (gulp)
5. Naviguer : http://localhost:9005